<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
	use SoftDeletes;
    use \Venturecraft\Revisionable\RevisionableTrait;

	public static function boot()
    {
        parent::boot();
    }

    static function list()
    {
        $arr = array();
        return $arr;
    }
}
